package sad.clinic.domian.entity;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;


public class ShiftDAO{
	private static String tableName="shift";
	private static int ER_NOT_UNIQ_KEY=23000;
	public static Shift getShiftById(Long id)throws IOException{
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * form ? where id = ?");
			statement.setString(1,tableName);
			statement.setLong(2,id);
			ResultSet result = statement.executeQuery();
			if(result.next()){
				java.util.Date date=result.getDate("shift_date");
				Shift ret = new Shift (
						DoctorDAO.getDoctorById(result.getLong("doctor_id")),
						new GregorianCalendar(date.getYear()+1900,
							date.getMonth()+1,
							date.getDate()),
						result.getInt("capacity"),
						Enum.valueOf(Shift.TimeOfDay.class,result.getString("time_of_day")),
						id
						);
				return ret;
			}
			return null;
		}catch(Exception e){
			System.err.println(e.getMessage());
			throw new IOException("خطا در ارتباط با پایگاه داده");
		}
	}
	public static void saveShift(Shift shift) throws IOException,InvalidDataException{
		if(shift.getId()!= null){
			System.err.println("Warning: id given. updating instead!");
			updateShift(shift);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into ?(doctor_id,shift_date,capacity,time_of_day) values(?,?,?,?)" );
			statement.setString(1,tableName);
			statement.setLong(2,shift.getDoctor().getId());
			statement.setDate(3,new java.sql.Date(shift.getDate().getTime().getTime()));
			statement.setInt (4,shift.getCapacity());
			statement.execute();
			Statement idStm = connection.createStatement();
			ResultSet result= idStm.executeQuery("select LAST_INSERT_ID()");
                        result.next();
			shift.setId(result.getLong(1));
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("نوبت مورد نظر پر است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void updateShift(Shift shift) throws IOException,InvalidDataException{
		if(shift.getId()== null){
			System.err.println("Warning: Id not given. Saving instead");
			saveShift(shift);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("update ? set(doctor_id=?,shift_date=?,capacity=?,time_of_day=?) where id=?" );
			statement.setString(1,tableName);
			statement.setLong(2,shift.getDoctor().getId());
			statement.setDate(3,new java.sql.Date(shift.getDate().getTime().getTime()));
			statement.setInt (4,shift.getCapacity());
			statement.setLong (5,shift.getId());
			statement.execute();
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("نوبت مورد نظر پر است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void removeShift(Shift shift) throws IOException{
		if(shift.getId()== null){
			System.err.println("Warning: Id not given. Skipping");
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("delete form ?  where id=?" );
			statement.setString(1,tableName);
			statement.setLong(2,shift.getId());
			statement.execute();
		}catch(SQLException e){
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static ArrayList<Shift> getShiftsByExpertiseInInterval(String expertise ,GregorianCalendar from, GregorianCalendar to)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? sh,(select id from ? where expertise=?) ex where ex.id= sh.doctor and shift_date >=? and shift_date <=? order by shift_date,time_of_day");
			statement.setString(1,tableName);
                        statement.setString(2,DoctorDAO.getTableName());
                        statement.setString(3, expertise);
			statement.setDate (4,new java.sql.Date(from.getTime().getTime()));
			statement.setDate  (5,new java.sql.Date(to.getTime().getTime()));
			ResultSet result = statement.executeQuery();
			ArrayList<Shift> ret= new ArrayList<Shift>();
			while(result.next()){
				java.util.Date date=result.getDate("shift_date");
				Shift now = new Shift (
						DoctorDAO.getDoctorById(result.getLong("doctor_id")),
						new GregorianCalendar(date.getYear()+1900,
							date.getMonth()+1,
							date.getDate()),
						result.getInt("capacity"),
						Enum.valueOf(Shift.TimeOfDay.class,result.getString("time_of_day")),
						result.getLong("id")
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}
	}
        public static ArrayList<Shift> getShiftsByDoctorInInterval(Doctor doctor ,GregorianCalendar from, GregorianCalendar to)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? where doctor_id=? and shift_date >=? and shift_date <=? order by shift_date,time_of_day");
			statement.setString(1,tableName);
			statement.setLong(2,doctor.getId());
			statement.setDate (3,new java.sql.Date(from.getTime().getTime()));
			statement.setDate  (4,new java.sql.Date(to.getTime().getTime()));
			ResultSet result = statement.executeQuery();
			ArrayList<Shift> ret= new ArrayList<Shift>();
			while(result.next()){
				java.util.Date date=result.getDate("shift_date");
				Shift now = new Shift (
						doctor,
						new GregorianCalendar(date.getYear()+1900,
							date.getMonth()+1,
							date.getDate()),
						result.getInt("capacity"),
						Enum.valueOf(Shift.TimeOfDay.class,result.getString("time_of_day")),
						result.getLong("id")
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}
	}
}
