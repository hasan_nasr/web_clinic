create database clinic;
use clinic;
create table if not exists doctor (id Long primary key,
		first_name varchar(256),
		last_name varchar(256),
		idNumber Long unique,
		address VARCHAR(512),
		phoneNumber varchar(32)
		expertise varchar(64));
create table if not exists patient (id Long primary key,
		first_name varchar(256),
		last_name varchar(256),
		idNumber Long unique,
		address VARCHAR(512),
		phoneNumber varchar(32));
create table if not exists shift( id Long primary key,
		doctor_id Long REFERENCES doctor(id),
		shift_date Date,
		time_of_day Char(16),
		capacity Integer)
create table if not exists meeting (id Long primary key,
		shift_id Long REFERENCES shift(id),
		patient_id Long REFERENCES patient(patient_id),
		tern Integer
		status CHAR(16));
