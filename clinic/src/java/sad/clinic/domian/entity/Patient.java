/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sad.clinic.domian.entity;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hossein
 */

public class Patient extends Person{

        ArrayList<Meeting> meetings;
        public Patient() {
        }

        Patient(Long idNumber, String firstName, String lastName, String Address, String phoneNumber, Long id) {
                super(idNumber, firstName, lastName, Address, phoneNumber, id);
        }

        public Patient(Long idNumber, String firstName, String lastName, String Address, String phoneNumber) {
                super(idNumber, firstName, lastName, Address, phoneNumber);
        }
        public void save() throws IOException, InvalidDataException{
                PatientDAO.savePatient(this);
        }
        public ArrayList<Meeting> getMeetings() throws IOException{
                if(meetings==null){
                        meetings=MeetingDAO.getMeetingsByPatient(this);
                }
                return meetings;
        }
}
