/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sad.clinic.domian.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author hossein
 */
public class Doctor extends Person{

        public class ShiftManager {
                //ArrayList<Shift> shifts=null;
                public ArrayList<Shift>getShiftsByDate(GregorianCalendar from,GregorianCalendar to) throws IOException{
                //        if(shifts==null)
                                return ShiftDAO.getShiftsByDoctorInInterval(Doctor.this,from,to);
                //        return shifts;
                }
                public void addShift(GregorianCalendar gc,TimeOfDay t,Integer ca) throws IOException, InvalidDataException{
                        Shift s=new Shift(Doctor.this, gc, ca, t);
                        s.save();
                }
        }
        
        String expertise;
        ShiftManager shiftManager;
        
        public ShiftManager getShifts(){
                if(shiftManager==null){
                        return new ShiftManager();
                }
                return shiftManager;
        }
        
        public void save() throws IOException, InvalidDataException{
                if(this.id==null){
                        DoctorDAO.saveDoctor(this);
                }
                else{
                        DoctorDAO.updateDoctor(this);
                }
        }

        public Doctor(Long idNumber, String firstName, String lastName, String Address, String phoneNumber, String expertise, Long id) {
                super(idNumber, firstName, lastName, Address, phoneNumber, id);
                this.expertise = expertise;
        }

        public Doctor(Long idNumber, String firstName, String lastName, String Address, String phoneNumber, String expertise) {
                super(idNumber, firstName, lastName, Address, phoneNumber);
                this.expertise = expertise;
        }

        public Doctor(String expertise) {
        }
        
        public String getExpertise() {
                return expertise;
        }

        public void setExpertise(String expertise) {
                this.expertise = expertise;
        }

        
}
