/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sad.clinic.domian.entity;

import java.io.IOException;

/**
 *
 * @author hossein
 */
public class Meeting {
        Long id=null;
        Shift shift;
        Patient patient;
        Integer Number;
        Status meetingState;
        enum Status {
                NotVisited,Visited,Canceled
        };
        public void save() throws InvalidDataException, IOException{
                if(id==null){
                        MeetingDAO.saveMeeting(this);
                }
                else{
                        MeetingDAO.updateMeeting(this);
                }
        }
        public Meeting(Shift shift, Patient patient, Integer Number, Status meetingState) {
                this.shift = shift;
                this.patient = patient;
                this.Number = Number;
                this.meetingState = meetingState;
        }

        public Meeting(Shift shift, Patient patient, Integer Number, Status meetingState,Long id) {
                this.shift = shift;
                this.patient = patient;
                this.Number = Number;
                this.meetingState = meetingState;
                this.id=id;
        }

        public Integer getNumber() {
                return Number;
        }

        public void setNumber(Integer Number) {
                this.Number = Number;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Status getStatus() {
                return meetingState;
        }

        public void setStatus(Status meetingState) {
                this.meetingState = meetingState;
        }

        public Patient getPatient() {
                return patient;
        }

        public void setPatient(Patient patient) {
                this.patient = patient;
        }

        public Shift getShift() {
                return shift;
        }

        public void setShift(Shift shift) {
                this.shift = shift;
        }
        
}
