/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sad.clinic.domian.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import sad.clinic.domian.entity.Meeting.Status;

/**
 *
 * @author hossein
 */
public class Shift {
        
        enum TimeOfDay {
                Morning,Evening,Midnight
        }

        Long id=null;
        Doctor doctor;
        GregorianCalendar date;
        Integer capacity;
        TimeOfDay timeOfDay;
        Integer size=0;
        ArrayList<Meeting> meetings=null;
        public Shift(Doctor doctor, GregorianCalendar date, Integer capacity, TimeOfDay timeOfDay) {
                this.doctor = doctor;
                this.date = date;
                this.capacity = capacity;
                this.timeOfDay = timeOfDay;
        }
        
        public Shift(Doctor doctor, GregorianCalendar date, Integer capacity, TimeOfDay timeOfDay, Long id) {
                this.doctor = doctor;
                this.date = date;
                this.capacity = capacity;
                this.timeOfDay = timeOfDay;
                this.size=size;
                this.id=id;
        }
        
        public ArrayList<Meeting> getMeetings() throws IOException{
                if(meetings==null){
                        meetings=MeetingDAO.getMeetingsByShift(this);
                }
                return meetings;
        }
        public void addMeeting(Patient p) throws ShiftFullException, IOException, InvalidDataException{
                if(size>=capacity){
                        throw new ShiftFullException("ظرفیت این شیفت پر است");
                }
                Meeting m=new Meeting(this, p, size, Status.NotVisited);
                size++;
                if(meetings!=null){
                        meetings.add(m);
                }
                MeetingDAO.saveMeeting(m);
        }
        public void cancel () throws ShiftIsNotEmpty, IOException{
                if(size==0){
                        ShiftDAO.removeShift(this);
                }
                else{
                        throw new ShiftIsNotEmpty("نمی‌یتوانید این شیفت را حذف کنید چون این شیفت خالی نمی‌باشد");
                }
        }
        public void save() throws IOException, InvalidDataException{
                if(id==null){
                        ShiftDAO.saveShift(this);
                }
                else{
                        ShiftDAO.updateShift(this);
                }
                
        }
        
        public Integer getCapacity() {
                return capacity;
        }

        public void setCapacity(Integer capacity) {
                this.capacity = capacity;
        }

        public GregorianCalendar getDate() {
                return date;
        }

        public void setDate(GregorianCalendar date) {
                this.date = date;
        }

        public Doctor getDoctor() {
                return doctor;
        }

        public void setDoctor(Doctor doctor) {
                this.doctor = doctor;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public TimeOfDay getTimeOfDay() {
                return timeOfDay;
        }

        public void setTimeOfDay(TimeOfDay timeOfDay) {
                this.timeOfDay = timeOfDay;
        }

        public Integer getSize() {
                return size;
        }

        public void setSize(Integer size) {
                this.size = size;
        }
        
        
}
