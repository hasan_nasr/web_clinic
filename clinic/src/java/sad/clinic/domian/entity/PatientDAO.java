package sad.clinic.domian.entity;


import java.sql.*;


public class PatientDAO{
	private static String tableName="patient";
	public static Patient getPatientById(Long id){
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * form ? where id = ?");
			statement.setString(1,tableName);
			statement.setLong(2,id);
			ResultSet result = statement.executeQuery();
			if(result.next()){
				Patient ret = new Patient(
						result.getLong("idNumber"),
						result.getString("firstName"),
						result.getString("lastName"),
						result.getString("address"),
						result.getString("phoneNumber"),
						result.getLong("id"),
						);
				return ret;
			}
			return null;
		}catch(Exception e){
			System.err.println(e.getMessage());
			return null
		}
	}
	public static void savePatient(Patient patient) throws IOException,InvalidDataException{
		if(patient.getId()!= null){
			System.err.println("Warning: id given. updating instead!");
			updatePatient(patient);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into ?(idNumber,firstName,lastName,address,phoneNumber) values(?,?,?,?,?);
			statement.setString(1,tableName);
			statement.setLong(2,patient.getIdNumber());
			statement.setString(3,patient.getFirstName());
			statement.setString(4,patient.getLastName());
			statement.setString(5,patient.getAddress());
			statement.setString(6,patient.getPhoneNumber());
			statement.execute();
			Statement idStm = Connection.getStatement("select LAST_INSERT_ID()");
			ResultSet result= idStm.executeQuery();
			Patient.setId(result.next().getLong(1));
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("شناسه تکراری است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void updatePatient(Patient patient) throws IOException,InvalidDataException{
		if(patient.getId()== null){
			System.err.println("Warning: Id not given. Saving instead");
			savePatient(patient);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("update ? set (idNumber=?,firstName=?,lastName=?,address=?,phoneNumber=?) where id=?" );
			statement.setString(1,tableName);
			statement.setLong(2,patient.getIdNumber());
			statement.setString(3,patient.getFirstName());
			statement.setString(4,patient.getLastName());
			statement.setString(5,patient.getAddress());
			statement.setString(6,patient.getPhoneNumber());
			statement.setString(7,patient.getId());
			statement.execute();
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("شناسه تکراری است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/*
	public static ArrayList<Patient> getPatientByExpertise(String expertise)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? where expertise=?");
			statement.setString(1,tableName);
			statement.setString(2,expertise);
			ResultSet result = statement.executeQuery();
			ArrayList<Patient> ret= new ArrayList<Patient>();
			while(result.next()){
				Patient now = new Patient(
						result.getLong("idNumber"),
						result.getString("firstName"),
						result.getString("lastName"),
						result.getString("address"),
						result.getString("phoneNumber"),
						result.getString("expertise"),
						result.getLong("id"),
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw IOException("خطا در ارتبات با پایگاه داده");
		}
	}
	*/
}
