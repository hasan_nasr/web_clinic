
package sad.clinic.domian.entity;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;


public class DoctorDAO{
	private static String tableName="doctor";
        private static final int ER_NOT_UNIQ_KEY=23000;
	public static Doctor getDoctorById(long id){
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * form ? where id = ?");
			statement.setString(1,tableName);
			statement.setLong(2,id);
			ResultSet result = statement.executeQuery();
			if(result.next()){
				Doctor ret = new Doctor(
						result.getLong("idNumber"),
						result.getString("firstName"),
						result.getString("lastName"),
						result.getString("address"),
						result.getString("phoneNumber"),
						result.getString("expertise"),
						result.getLong("id")
						);
				return ret;
			}
			return null;
		}catch(Exception e){
			System.err.println(e.getMessage());
			return null;
		}
	}
	public static void saveDoctor(Doctor doctor) throws IOException,InvalidDataException{
		if(doctor.getId()!= null){
			System.err.println("Warning: id given. updating instead!");
			updateDoctor(doctor);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into ?(idNumber,firstName,lastName,address,phoneNumber,expertise) values(?,?,?,?,?,?)" );
			statement.setString(1,tableName);
			statement.setLong(2,doctor.getIdNumber());
			statement.setString(3,doctor.getFirstName());
			statement.setString(4,doctor.getLastName());
			statement.setString(5,doctor.getAddress());
			statement.setString(6,doctor.getPhoneNumber());
			statement.setString(7,doctor.getExpertise());
			statement.execute();
			Statement idStm = connection.createStatement();
			ResultSet result= idStm.executeQuery("select LAST_INSERT_ID()");
                        result.next();
			doctor.setId(result.getLong(1));
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("شناسه تکراری است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void updateDoctor(Doctor doctor) throws IOException,InvalidDataException{
		if(doctor.getId()== null){
			System.err.println("Warning: Id not given. Saving instead");
			saveDoctor(doctor);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("update ? set (idNumber=?,firstName=?,lastName=?,address=?,phoneNumber=?,expertise=?) where id=?" );
			statement.setString(1,tableName);
			statement.setLong(2,doctor.getIdNumber());
			statement.setString(3,doctor.getFirstName());
			statement.setString(4,doctor.getLastName());
			statement.setString(5,doctor.getAddress());
			statement.setString(6,doctor.getPhoneNumber());
			statement.setString(7,doctor.getExpertise());
			statement.setLong(8,doctor.getId());
			statement.execute();
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("شناسه تکراری است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static ArrayList<Doctor> getDoctorByExpertise(String expertise)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? where expertise=?");
			statement.setString(1,tableName);
			statement.setString(2,expertise);
			ResultSet result = statement.executeQuery();
			ArrayList<Doctor> ret= new ArrayList<Doctor>();
			while(result.next()){
				Doctor now = new Doctor(
						result.getLong("idNumber"),
						result.getString("firstName"),
						result.getString("lastName"),
						result.getString("address"),
						result.getString("phoneNumber"),
						result.getString("expertise"),
						result.getLong("id")
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}
	}
}
