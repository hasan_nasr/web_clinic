
package sad.clinic.domian.entity;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;


public class MeetingDAO{
	private static String tableName="meeting";
	private static long ER_NOT_UNIQ_KEY=23000;
	public static Meeting getMeetingById(long id)throws IOException{
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * form ? where id = ?");
			statement.setString(1,tableName);
			statement.setLong(2,id);
			ResultSet result = statement.executeQuery();
			if(result.next()){
				Meeting ret = new Meeting (
						ShiftDAO.getShiftById(result.getLong("shift_id")),
						PatientDAO.getPatientById(result.getLong("patient_id")),
						result.getInt("number"),
						Enum.valueOf(Meeting.Status.class,result.getString("status"))
						);
				return ret;
			}
			return null;
		}catch(Exception e){
			System.err.println(e.getMessage());
			throw new IOException("خطا در ارتباط با پایگاه داده");
		}
	}
	public static void saveMeeting(Meeting meeting) throws IOException,InvalidDataException{
		if(meeting.getId()!= null){
			System.err.println("Warning: id given. updating instead!");
			updateMeeting(meeting);
		}
		try{

			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("insert into ?(shift_id,patient_id,number,status) values(?,?,?,?)" );
			statement.setString(1,tableName);
			statement.setLong(2,meeting.getShift().getId());
			statement.setLong(3,meeting.getPatient().getId());
			statement.setInt(4,meeting.getNumber());
			statement.setString(5,meeting.getStatus().toString());
			statement.execute();
			Statement idStm = connection.createStatement();
			ResultSet result= idStm.executeQuery("select LAST_INSERT_ID()");
                        result.next();
			meeting.setId(result.getLong(1));
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("نوبت مورد نظر پر است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void updateMeeting(Meeting meeting) throws IOException,InvalidDataException{
		if(meeting.getId()== null){
			System.err.println("Warning: Id not given. Saving instead");
			saveMeeting(meeting);
		}
		try{
			Connection connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("update ? set(shift_id=?,patient_id=?,number=?,status=?) whrere id=?" );
			statement.setString(1,tableName);
			statement.setLong(2,meeting.getShift().getId());
			statement.setLong(3,meeting.getPatient().getId());
			statement.setInt(4,meeting.getNumber());
			statement.setString(5,meeting.getStatus().toString());
			statement.setLong(6,meeting.getId());
			statement.execute();
		}catch(SQLException e){
			if(e.getErrorCode() == ER_NOT_UNIQ_KEY)
				throw new InvalidDataException("نوبت مورد نظر پر است");
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static ArrayList<Meeting> getMeetingsByShift(Shift shift)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? where shift_id=?");
			statement.setString(1,tableName);
			statement.setLong(2,shift.getId());
			ResultSet result = statement.executeQuery();
			ArrayList<Meeting> ret= new ArrayList<Meeting>();
			while(result.next()){
				Meeting now = new Meeting(
						shift,
						PatientDAO.getPatientById(result.getLong("patient_id")),
						result.getInt("number"),
						Enum.valueOf(Meeting.Status.class,result.getString("status"))
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}
	}
        public static ArrayList<Meeting> getMeetingsByPatient(Patient patient)throws IOException{
		try{
			Connection connection=DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement("select * from ? where patient_id=?");
			statement.setString(1,tableName);
			statement.setLong(2,patient.getId());
			ResultSet result = statement.executeQuery();
			ArrayList<Meeting> ret= new ArrayList<Meeting>();
			while(result.next()){
				Meeting now = new Meeting(
						ShiftDAO.getShiftById(result.getLong("shift_id")),
                                                patient,
						result.getInt("number"),
						Enum.valueOf(Meeting.Status.class,result.getString("status"))
						);
				ret.add(now);
			}
			return ret;
		}catch(SQLException e){
			e.printStackTrace();
			throw new IOException("خطا در ارتبات با پایگاه داده");
		}
	}
}
