/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sad.clinic.domian.entity;

/**
 *
 * @author hossein
 */
public class Person {
        Long id=null,idNumber;
        String firstName,lastName,Address,phoneNumber;

        public String getAddress() {
                return Address;
        }

        public void setAddress(String Address) {
                this.Address = Address;
        }

        public String getFirstName() {
                return firstName;
        }

        public void setFirstName(String firstName) {
                this.firstName = firstName;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Long getIdNumber() {
                return idNumber;
        }

        public void setIdNumber(Long idNumber) {
                this.idNumber = idNumber;
        }

        public String getLastName() {
                return lastName;
        }

        public void setLastName(String lastName) {
                this.lastName = lastName;
        }

        public String getPhoneNumber() {
                return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
        }

        public Person() {
        }

        public Person(Long idNumber, String firstName, String lastName, String Address, String phoneNumber) {
                this.idNumber = idNumber;
                this.firstName = firstName;
                this.lastName = lastName;
                this.Address = Address;
                this.phoneNumber = phoneNumber;
        }

        public Person( Long idNumber, String firstName, String lastName, String Address, String phoneNumber,Long id) {
                this.id = id;
                this.idNumber = idNumber;
                this.firstName = firstName;
                this.lastName = lastName;
                this.Address = Address;
                this.phoneNumber = phoneNumber;
        }

}
